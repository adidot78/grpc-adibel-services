module adibel/services

go 1.14

require (
	adibel/core v0.0.0-00010101000000-000000000000
	github.com/joho/godotenv v1.3.0
	github.com/micro/go-micro v1.18.0
	github.com/micro/go-plugins/registry/kubernetes v0.0.0-20200119172437-4fe21aa238fd
)

replace adibel/core => ../../core
