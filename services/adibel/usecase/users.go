package usecase

import (
	core "adibel/core/proto"
	ut "adibel/core/utils"
)

// UserRegistration ...
func (uc *UseCase) UserRegistration(data *core.InputRegistrationUser) (string, error) {
	ut.SendLogInfo("Users UserRegistration UseCase")

	id, err := uc.Repository.UserRegistration(data)
	if err != nil {
		return "", err
	}

	return id, nil
}
