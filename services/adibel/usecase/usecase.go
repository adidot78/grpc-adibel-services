package usecase

import (
	"adibel/services/repository"
)

type UseCase struct {
	Repository *repository.AbstractRepository
}

func NewUseCase(repo *repository.AbstractRepository) *UseCase {
	return &UseCase{
		Repository: repo,
	}
}
