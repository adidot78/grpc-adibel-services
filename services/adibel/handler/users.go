package handler

import (
	"context"

	core "adibel/core/proto"
)

type UserHandler struct{}

// UserRegistration...
func (*UserHandler) UserRegistration(
	ctx context.Context, in *core.InputRegistrationUser, out *core.ResponseUserRegistration,
) error {
	id, err := GetUseCase().UserRegistration(in)
	out.ID = id

	return err
}
