package handler

import (
	"sync"

	rp "adibel/services/repository"
	"adibel/services/usecase"
)

var uc *usecase.UseCase
var oneUc sync.Once

// GetUseCase...
func GetUseCase() *usecase.UseCase {
	oneUc.Do(func() {
		uc = usecase.NewUseCase(getRepository())
	})

	return uc
}

var repo *rp.AbstractRepository
var repoOnce sync.Once

// getRepository...
func getRepository() *rp.AbstractRepository {
	repoOnce.Do(func() {
		repo = rp.NewRepository()
	})

	return repo
}
