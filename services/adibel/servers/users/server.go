package main

import (
	"github.com/joho/godotenv"
	"github.com/micro/go-micro"
	"github.com/micro/go-micro/service/grpc"
	"github.com/micro/go-micro/util/log"

	coreclient "adibel/core/client"
	core "adibel/core/proto"
	ut "adibel/core/utils"
	h "adibel/services/handler"
)

func init() {
	if err := godotenv.Load(); err != nil {
		ut.SendLogInfo("No .env file found ..")
	}

	ut.SendLogInfo("Data Source : " + ut.GetDatasourceInfo())
}

func main() {
	service := grpc.NewService(
		micro.Name(coreclient.UserService),
		micro.Version("latest"),
	)

	service.Init()
	core.RegisterAdibelUsersHandler(service.Server(), new(h.UserHandler))

	if err := service.Run(); err != nil {
		log.Fatal(err)
	}
}
