package repository

import (
	core "adibel/core/proto"
	ut "adibel/core/utils"
)

func (ar *AbstractRepository) UserRegistration(data *core.InputRegistrationUser) (string, error) {
	var (
		err     error
		id      string
		errText = "Adibel.Services.UserRegistration"
	)

	if id, err = ar.UserRegistrationQuery(data); err != nil {
		return "", ut.SendLogError(errText+"Query", err)
	}

	return id, nil
}
