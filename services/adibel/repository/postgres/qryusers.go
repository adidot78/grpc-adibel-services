package postgres

import (
	"fmt"

	core "adibel/core/proto"
	ut "adibel/core/utils"
)

// UserRegistrationQuery ...
func (pr *Repository) UserRegistrationQuery(data *core.InputRegistrationUser) (string, error) {
	userID := ut.GenerateUUID()

	queryInsert := fmt.Sprintf(`
		INSERT INTO users(id, first_name, last_name, email, username, phone, address, password)
		VALUES($1, $2, $3, $4, $5, $6, $7, $8)
	`)

	err := pr.Connection.ExecuteDBInsert(queryInsert, userID, data.Fn, data.Ln, data.Em, data.Uname, data.Phone, data.Addr, data.Pass)
	if err != nil {
		return "", err
	}
	return userID, nil
}
