package postgres

import (
	conn "adibel/core/connection"
)

type Repository struct {
	Connection conn.Connection
}
