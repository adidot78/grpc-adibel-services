package repository

import (
	core "adibel/core/proto"
	ut "adibel/core/utils"
	p "adibel/services/repository/postgres"
)

type Repository interface {
	UserRegistrationQuery(data *core.InputRegistrationUser) (string, error)
}

type AbstractRepository struct {
	Repository
}

func NewRepository() *AbstractRepository {
	newRepo := &AbstractRepository{}

	if ut.GetDatasourceInfo() == ut.Postgres {
		newRepo.Repository = &p.Repository{
			Connection: ut.GetPostgresHandler(),
		}
	} else {
		newRepo.Repository = nil
	}

	return newRepo
}
