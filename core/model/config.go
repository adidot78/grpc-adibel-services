package model

import (
	"time"
)

type ApplicationConfig struct {
	AppName      string
	GRPCTimeout  time.Duration
	CacheExpiry  time.Duration
	CacheCleanup time.Duration
	FrontendURI  string
}
