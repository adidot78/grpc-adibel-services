package connection

import (
	"database/sql"
	"fmt"
	"os"
	"strconv"
	"time"

	common "adibel/core/model"

	"github.com/jmoiron/sqlx"
	"github.com/micro/go-micro/util/log"

	// Postgres dialect
	_ "github.com/lib/pq"
)

type Connection interface {
	ExecuteDBQuery(query string) ([][]string, error)
	ExecuteDBInsert(query string, args ...interface{}) error
	DB() *sqlx.DB
}

type connection struct {
	cfg *common.ApplicationConfig
	db  *sqlx.DB
}

func NewConnection(cfg *common.ApplicationConfig) Connection {
	return &connection{
		cfg,
		setDbConnection(cfg),
	}
}

func (conn *connection) ExecuteDBQuery(query string) ([][]string, error) {
	var records [][]string

	rows, errConnection := conn.db.Query(query)
	if errConnection == nil {
		records = convertToCsv(rows)
	}

	if rows != nil {
		defer rows.Close()
	}

	return records, errConnection
}

func (conn *connection) ExecuteDBInsert(qapi string, args ...interface{}) error {
	_, errConnection := conn.db.Exec(qapi, args...)

	if errConnection != nil {
		log.Info("Failed execute Query", errConnection)
	}

	return errConnection
}

func (conn *connection) DB() *sqlx.DB {
	return conn.db
}

func convertToCsv(rows *sql.Rows) (results [][]string) {
	cols, err := rows.Columns()
	if err != nil {
		log.Info("Failed to get columns", err)
		return
	}

	// Result is your slice string.
	rawResult := make([][]byte, len(cols))
	results = append(results, []string{"HEADER"}) //static values on the first index

	dest := make([]interface{}, len(cols)) // A temporary interface{} slice
	for i := range rawResult {
		dest[i] = &rawResult[i] // Put pointers to each string in the interface slice
	}

	for rows.Next() {
		var result []string
		err = rows.Scan(dest...)
		if err != nil {
			log.Info("Failed to scan row", err)
			return
		}

		for _, raw := range rawResult {
			if raw == nil {
				result = append(result, "")
			} else {
				result = append(result, string(raw))
			}
		}

		results = append(results, result)
	}

	return results
}

func setDbConnection(config *common.ApplicationConfig) *sqlx.DB {
	host := os.Getenv("DB_HOST")
	port := os.Getenv("DB_PORT")
	user := os.Getenv("DB_USER")
	dbname := os.Getenv("DB_NAME")
	pass := os.Getenv("DB_PASS")
	timeout := os.Getenv("DB_TIMEOUT")
	idleConn, _ := strconv.Atoi(os.Getenv("DB_IDLE_CONN"))
	openConn, _ := strconv.Atoi(os.Getenv("DB_OPEN_CONN"))
	connLifetime, _ := strconv.Atoi(os.Getenv("DB_CONN_LIFETIME"))

	connStr := fmt.Sprintf(
		"host=%s port=%s user=%s dbname=%s password=%s connect_timeout=%s sslmode=disable",
		host, port, user, dbname, pass, timeout,
	)

	connDB, connError := sqlx.Open("postgres", connStr)
	if connError != nil {
		log.Errorf("Error establishing connection to %s database: %v", dbname, connError)
		panic(connError)
	}

	pingError := connDB.Ping()
	if pingError != nil {
		log.Errorf("Error connecting to %s database: %s", dbname, pingError)
		panic(pingError)
	}

	connDB.SetMaxIdleConns(idleConn)
	connDB.SetMaxOpenConns(openConn)
	connDB.SetConnMaxLifetime(time.Duration(connLifetime) * time.Second)

	return connDB
}
