package utils

import (
	"os"
	"strconv"
	"sync"

	"github.com/jinzhu/copier"
	"github.com/micro/go-micro/errors"
	"github.com/micro/go-micro/util/log"

	config "adibel/core/config"
	connection "adibel/core/connection"

	uuid "github.com/satori/uuid"
)

const (
	Postgres = "postgresql"
)

var (
	conn    connection.Connection
	oneConn sync.Once
)

func GetPostgresHandler() connection.Connection {
	oneConn.Do(func() {
		conn = connection.NewConnection(config.GetApplicationConfig())
	})

	return conn
}

func CopyObject(from interface{}, to interface{}) error {
	if from == nil {
		to = nil
		return nil
	}

	return copier.Copy(to, from)
}

func SendLogError(id string, err error) error {
	log.Log(errors.BadRequest(id, err.Error()))
	return err
}

func SendLogInfo(msg ...string) {
	log.Info(msg)
}

func SendLogDebug(msg ...string) {
	log.Debug(msg)
}

// StringToBool ...
func StringToBool(input string) (bool, error) {
	result, _ := strconv.ParseBool(input)
	return result, nil
}

// StringToInt ...
func StringToInt(input string) (int, error) {
	result, _ := strconv.Atoi(input)
	return result, nil
}

// StringToInt32 ...
func StringToInt32(input string) (int32, error) {
	result, _ := strconv.Atoi(input)
	return int32(result), nil
}

//StringToFloat32 ...
func StringToFloat32(input string) (float32, error) {
	result, _ := strconv.ParseFloat(input, 32)
	return float32(result), nil
}

//StringToFloat64 ...
func StringToFloat64(input string) (float64, error) {
	result, _ := strconv.ParseFloat(input, 64)
	return result, nil
}

// BooleanToString ...
func BooleanToString(input bool) (string, error) {
	result := strconv.FormatBool(input)
	return result, nil
}

func GetDatasourceInfo() string {
	if typ := os.Getenv("REPO_TYPE"); typ != "" {
		return typ
	}

	return Postgres
}

func GenerateUUID() string {
	return uuid.NewV4().String()
}
