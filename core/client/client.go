package client

import (
	"sync"
	"time"

	configFile "adibel/core/config"
	service "adibel/core/proto"

	"github.com/micro/go-micro/client"
	grpcclient "github.com/micro/go-micro/client/grpc"
)

const (
	UserService = "go.micro.srv.user.service"
)

func getClientOptions() []client.Option {
	config := configFile.GetApplicationConfig()
	options := []client.Option{
		client.RequestTimeout(config.GRPCTimeout * time.Second),
	}

	return options
}

var (
	userOnce    sync.Once
	userService service.AdibelUsersService
)

func GetAdibelUserService() service.AdibelUsersService {
	userOnce.Do(func() {
		options := getClientOptions()
		userService = service.NewAdibelUsersService(UserService, grpcclient.NewClient(options...))
	})

	return userService
}
