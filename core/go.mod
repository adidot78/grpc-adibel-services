module adibel/core

go 1.14

require (
	github.com/golang/protobuf v1.4.3
	github.com/jinzhu/copier v0.2.4
	github.com/jmoiron/sqlx v1.3.1
	github.com/lib/pq v1.2.0
	github.com/micro/go-micro v1.18.0
	github.com/satori/uuid v1.2.0
)
