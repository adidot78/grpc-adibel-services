module adibel/api

go 1.14

require (
	adibel/core v0.0.0-00010101000000-000000000000
	github.com/99designs/gqlgen v0.13.0
	github.com/go-chi/chi v3.3.2+incompatible
	github.com/gorilla/websocket v1.4.2
	github.com/joho/godotenv v1.3.0
	github.com/satori/uuid v1.2.0 // indirect
	github.com/vektah/gqlparser/v2 v2.1.0
)

replace adibel/core => ../core
