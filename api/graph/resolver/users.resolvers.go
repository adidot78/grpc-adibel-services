package resolver

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"adibel/api/graph"
	"adibel/api/graph/model"
	"adibel/api/services"
	"context"
)

func (r *usersMutationResolver) Registration(ctx context.Context, obj *model.AbstractModel, request model.InputUserRegistration) (*model.ResponseUserRegistration, error) {
	id, err := services.UserRegistration(ctx, request)
	if err != nil {
		return nil, err
	}

	return id, nil
}

// UsersMutation returns graph.UsersMutationResolver implementation.
func (r *Resolver) UsersMutation() graph.UsersMutationResolver { return &usersMutationResolver{r} }

type usersMutationResolver struct{ *Resolver }
