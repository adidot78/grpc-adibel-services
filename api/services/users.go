package services

import (
	"context"

	model "adibel/api/graph/model"

	coreclient "adibel/core/client"
	servicemodel "adibel/core/proto"
	ut "adibel/core/utils"
)

// UserRegistration...
func UserRegistration(ctx context.Context, req model.InputUserRegistration) (result *model.ResponseUserRegistration, err error) {
	ut.SendLogInfo("Adibel UserRegistration API services...")
	request := new(servicemodel.InputRegistrationUser)
	if err := ut.CopyObject(req, &request); err != nil {
		return nil, err
	}

	service := coreclient.GetAdibelUserService()
	response, err := service.UserRegistration(
		ctx,
		request,
	)

	if err != nil {
		return nil, err
	}

	result = new(model.ResponseUserRegistration)
	err = ut.CopyObject(response, result)

	return result, err
}
